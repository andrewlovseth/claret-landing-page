<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="info">
		<div class="wrapper">
			
			<div class="note">
				<h3><?php the_field('note'); ?></h3>
			</div>

			<div class="address">
				<?php the_field('location'); ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>